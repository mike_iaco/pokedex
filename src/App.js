import { Switch, Route } from "react-router-dom";
import Pokemon from "./components/Pokemon";
import Home from "./components/Home";
import PokeDetail from "./components/PokeDetail";
import NotFound from "./components/404";

import "./App.scss";

function App() {
  return (
    <div className="App">
      <Switch>
        <Route path="/pokemon" exact>
          <Pokemon />
        </Route>
        <Route path="/:pokemon" exact component={PokeDetail} />
        <Route path="/" exact>
          <Home />
        </Route>

        {/* En caso de que no encontremos la ruta, entramos a Not Found */}
        <Route>
          <NotFound />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
