import React, { useState, useEffect } from "react";
import { Link, Redirect } from "react-router-dom";
import "./pokedetail.scss";

function PokeDetail({ match }) {
  const [pokemonDetail, setPokemonDetail] = useState([]);

  useEffect(() => {
    fetchName();
  }, []);

  const fetchName = () => {
    fetch(`https://pokeapi.co/api/v2/pokemon/${match.params.pokemon}`)
      .then(response => response.json())
      // .then(detail => console.log(detail));
      .then(detail => setPokemonDetail(detail));
      console.log(pokemonDetail)
  };

  return (
    <div className="App container-fluid">
      <div className="pokedex">
      <Link to="/">
        <img className="col-sm-4 col-xl-1"
          src="https://media.giphy.com/media/jQbinPsqqfg8GFxbQw/giphy.gif"
          alt="pokeball"
        />
        </Link>
        <h2>POKEDEX</h2>
      </div>
      <div className="container">
        <img
          className="border border-dark rounded-circle col-sm-6 col-xl-3 bg-dark"
          src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${
            match.params.pokemon
          }.png`}
          alt="pokemon"
        />
      </div>
      <div className="container border border-dark background font-weight-bold">
        ID: {pokemonDetail.id}
        <br />
        <br />
        Name: {pokemonDetail.name}
        <br />
        <br />
        Height: {pokemonDetail.height}
        <br />
        <br />
        Weight: {pokemonDetail.weight}
        <br />
        <br />
        Stats: {pokemonDetail.stats && pokemonDetail.stats[0].base_stat}
        <br />
        <br />
        Effort: {pokemonDetail.stats && pokemonDetail.stats[0].effort}
        <br />
        <br />
        Type: {pokemonDetail.types && pokemonDetail.types[0].type.name}
        <br />
        <br />
        Abilities:{" "}
        {pokemonDetail.abilities && pokemonDetail.abilities[0].ability.name}
      </div>
    </div>
  );
}

export default PokeDetail;
