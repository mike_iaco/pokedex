import "./home.scss";

import { Link, Redirect } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import Div100vh from 'react-div-100vh';

function Home() {
  return (
<Div100vh className="Home">
<div className="container-sm container-xl text-light">
<img className="col-sm-6 col-xl-4" src="https://media.giphy.com/media/8WzI3BftWU5Xy/giphy.gif" alt="pokemon"/>
</div>
<a href="#" class="btn btn-secondary btn-lg disabled mt-5" tabindex="-1" role="button" aria-disabled="true">Let's play with the Pokeball!!</a>
<div className="link">
<Link to="/pokemon">
<img className="col-sm-6 col-xl-6" src="https://media.giphy.com/media/DJM88aCmEeaNG/giphy.gif" alt="pokemon"/>
          </Link>
</div>
</Div100vh>
  );
}

export default Home;
