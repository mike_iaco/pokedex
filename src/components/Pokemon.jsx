import { useState, useEffect } from "react";
import "./pokemon.scss";
import Div100vh from 'react-div-100vh';
import { Link } from "react-router-dom";

function App() {
  const [result, setResult] = useState([]);
  const [poke, setPoke] = useState([]);
  const [isVisible, setIsVisible] = useState(false);
  const [limit, setLimit] = useState(0);


  useEffect(() => {
    fetch("https://pokeapi.co/api/v2/pokemon?limit=20&offset="+limit)
      .then((response) => response.json())
      .then((data) =>
          data.results.map((item) => {
            fetch(item.url)
              .then((response) => response.json())
              .then((allpokemon) => {
                console.log(allpokemon)
                setPoke((prevPoke) => [...prevPoke, allpokemon]);
                setIsVisible(true)
              });
          })
      );
  }, [limit]);

  function uppercase(str) {
    var array1 = str.split(" ");
    var newarray1 = [];

    for (var x = 0; x < array1.length; x++) {
      newarray1.push(array1[x].charAt(0).toUpperCase() + array1[x].slice(1));
    }
    return newarray1.join(" ");
  }

  function Addmorepokemons() {
    setLimit(limit + 20);
  }
  
  const orderPokemon = poke.sort((a, b) => {return a.id - b.id})

  return (
    <Div100vh className="vh-100 box">
      <button className="btn btn-primary col-6" onClick={() => setIsVisible(!isVisible)}>
      {!isVisible ? "Show" : "Hide" }
      </button>
      {isVisible ? (
        <div className="box">
        <div className="d-flex flex-wrap text-center ml-4">
          {orderPokemon.map((img, i) => (
            <div id={img.id} key={img.id}>
              <div className="card">
              <Link style={{ color: '#000008', textDecoration: 'none' }} to={`${img.id}`}>
                <img className="col-10" src={img.sprites.front_default} alt="pokemon"/>
                <div>
                  <h5>{uppercase(img.name)}</h5>
                  <h6>Type: {uppercase(img.types[0].type.name)}</h6>
                </div>
                </Link>
              </div>
            </div>
            ))}
            <div className="container">
            {isVisible ? (<button className="btn btn-primary col-6" onClick={() => Addmorepokemons()}>Ver mas...</button>) : null}
            </div>
        </div>
        </div>
        
      ): null}
      </Div100vh>
  );
}

export default App;